/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.singidunum.projectKDP.deo.Suppliers;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nikola
 */
public class TableSuppliers implements Dao<Suppliers>{

    @Override
    public void create(Connection con, Suppliers dataObject) throws SQLException {
        String sql = "INSERT INTO Suppliers(SupplierId,SupplierName,ContactPerson,Address,Country,PostCode) VALUES (?,?,?,?,?,?);";
        PreparedStatement stm = con.prepareStatement(sql);
        stm.setInt(1, dataObject.getSupplierId());
        stm.setString(2, dataObject.getSupplierName());
        stm.setString(3, dataObject.getContactPerson());
        stm.setString(4, dataObject.getAddress());
        stm.setString(5, dataObject.getCity());
        stm.setString(6, dataObject.getPostCode());
        stm.execute();

    }

    @Override
    public void update(Connection con, Suppliers dataObject) throws SQLException 
    {
        String sql = "  UPDATE Suppliers  SET  SupplierId=?,SupplierName=?,ContactPerson=?,Address=?,Country=?,PostCode=? WHERE SupplierId=? ;";
        PreparedStatement stm = con.prepareStatement(sql);
        stm.setInt(1,dataObject.getSupplierId());
        stm.setString(2,dataObject.getSupplierName());
        stm.setString(3,dataObject.getContactPerson());
        stm.setString(4,dataObject.getAddress());
        stm.setString(5,dataObject.getCity());
        stm.setString(6,dataObject.getPostCode());
        stm.setInt(7,dataObject.getSupplierId());
        stm.execute();
     }

    @Override
    public void delete(Connection con, int dataObjectId) throws SQLException {
       
       String sql = "DELETE from Suppliers  where SupplierId= ? ;";
       PreparedStatement stm = con.prepareStatement(sql);
       stm.setInt(1,dataObjectId);
       stm.execute();
    }

    @Override
    public Suppliers find(Connection con, int dataObjectId) throws SQLException {
     
   String sql =  "  Select * from Suppliers where SupplierId = ? ;";
   PreparedStatement stm = con.prepareStatement(sql);
    stm.setInt(1, dataObjectId);   
    stm.execute();
    ResultSet r = stm.getResultSet();
   if( !r.isBeforeFirst() && r.getRow() == 0 )
       return null;
    r.next();
    
    return new Suppliers(dataObjectId,r.getString("SupplierName"),r.getString("ContactPerson"),r.getString("Address"),r.getString("Country"),r.getString("PostCode"));
    }

    @Override
    public List<Suppliers> findAll(Connection con) throws SQLException {
       List<Suppliers> ret = new ArrayList<Suppliers>();
       String sql =  "  Select * from Suppliers ;";
       PreparedStatement stm = con.prepareStatement(sql);
       stm.execute();
       ResultSet r = stm.getResultSet();
       while(r.next())
       {
           ret.add(new Suppliers(r.getInt("SupplierId"),r.getString("SupplierName"),r.getString("ContactPerson"),r.getString("Address"),r.getString("Country"),r.getString("PostCode")));
       }
       return ret;
    }
    
}
