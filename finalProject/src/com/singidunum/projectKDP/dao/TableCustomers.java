/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.singidunum.projectKDP.deo.Customers;
import com.singidunum.projectKDP.deo.DataClass;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nikola
 */
public class TableCustomers implements Dao<Customers>{

    @Override
    public void create(Connection con, Customers dataObject) throws SQLException {
        String sql =" INSERT INTO Customers(customerId,customerName,contactPerson,address,city,postCode) "
                + "   VALUES (?,?,?,?,?,?)";
       
        PreparedStatement stm = con.prepareStatement(sql);
        stm.setInt(1, dataObject.getCustomerId());
        stm.setString(2,dataObject.getCustomerName());
        stm.setString(3,dataObject.getContractPerson());
        stm.setString(4,dataObject.getAddress());
        stm.setString(5,dataObject.getCity());
        stm.setInt(6,dataObject.getPostCode());
        
        stm.execute();
        System.out.println("Customer created!");

    }

    @Override
    public void update(Connection con, Customers dataObject) throws SQLException {
        String sql = "UPDATE Customers" + 
        "  SET customerId=?,customerName=?,contactPerson=?,address=?,city=?,postCode=? " +
        "  WHERE customerId=? ;" ; 
        PreparedStatement stm = con.prepareStatement(sql);
        stm.setInt(1, dataObject.getCustomerId());
        stm.setString(2,dataObject.getCustomerName());
        stm.setString(3,dataObject.getContractPerson());
        stm.setString(4,dataObject.getAddress());
        stm.setString(5,dataObject.getCity());
        stm.setInt(6,dataObject.getPostCode());
        stm.setInt(7, dataObject.getCustomerId());
        
        stm.execute();
        System.out.println("Customer Updated!");

    }

    @Override
    public void delete(Connection con, int dataObjectId) throws SQLException {
       String sql = "DELETE from Customers "+
        "where CustomerId= ? ;";
       PreparedStatement stm = con.prepareStatement(sql);
       stm.setInt(1, dataObjectId);
       
       stm.execute();
       
    }

    @Override
    public Customers find(Connection con, int dataObjectId) throws SQLException {
        String sql = "Select * from Customers " +
                " where CustomerId = ? ;";
        PreparedStatement stm = con.prepareStatement(sql);
        stm.setInt(1, dataObjectId);
        stm.execute();
        //customerId,customerName,contactPerson,address,city,postCode
        ResultSet r = stm.getResultSet();
        if( !r.isBeforeFirst() && r.getRow() == 0 )
            return null;
        r.next();
        return new Customers(dataObjectId,r.getString("customerName"),r.getString("contactPerson")
        ,r.getString("address"),r.getString("city"),r.getInt("postCode"));
        
    }

    @Override
    public List<Customers> findAll(Connection con) throws SQLException {
        List<Customers> ret=new ArrayList<Customers>();
        
                String sql = "Select * from Customers " ;
        PreparedStatement stm = con.prepareStatement(sql);      
        stm.execute();
        ResultSet r = stm.getResultSet();
        while(r.next())
        ret.add(new Customers(r.getInt("customerId"),r.getString("customerName"),r.getString("contactPerson")
        ,r.getString("address"),r.getString("city"),r.getInt("postCode")));
        
        return ret;
    }
   

    
   
}
