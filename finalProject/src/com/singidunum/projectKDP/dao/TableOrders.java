/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.singidunum.projectKDP.deo.Orders;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nikola
 */
public class TableOrders implements Dao<Orders>{

    @Override
    public void create(Connection con, Orders dataObject) throws SQLException {
     String sql = "INSERT INTO Orders(orderId,orderDate,customerId,employeeId,shipperId) VALUES (?,?,?,?,?);";
     PreparedStatement stm = con.prepareStatement(sql);
    stm.setInt(1, dataObject.getOrderId());
    stm.setString(2, dataObject.getOrderDate().toString());
    stm.setInt(3, dataObject.getCustomerId());
    stm.setInt(4, dataObject.getEmployeeId());
    stm.setInt(5, dataObject.getShipperId());
    stm.execute();  
    }

    @Override
    public void update(Connection con, Orders dataObject) throws SQLException {
     String sql = "  UPDATE Orders  SET  OrderId=?,OrderDate=?,CustomerId=?,EmployeeId=?,ShipperId=? WHERE OrderId=? ;";
     PreparedStatement stm = con.prepareStatement(sql);
    stm.setInt(1,dataObject.getOrderId());
    stm.setString(2,dataObject.getOrderDate().toString());  
    stm.setInt(3,dataObject.getCustomerId());
    stm.setInt(4,dataObject.getEmployeeId());
    stm.setInt(5,dataObject.getShipperId());
    stm.setInt(6,dataObject.getOrderId());
    stm.execute();
    }

    @Override
    public void delete(Connection con, int dataObjectId) throws SQLException {
     String sql = "DELETE from Orders  where OrderId= ? ;";
     PreparedStatement stm = con.prepareStatement(sql);
     stm.setInt(1,dataObjectId);
     stm.execute();  
    }

    @Override
    public Orders find(Connection con, int dataObjectId) throws SQLException {
      String sql = "  Select * from Orders where OrderId = ? " ;
       PreparedStatement stm = con.prepareStatement(sql);
     stm.setInt(1, dataObjectId);
     stm.execute();
     ResultSet r = stm.getResultSet();
     if( !r.isBeforeFirst() && r.getRow() == 0 )
         return null;
     r.next();
     DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
      java.util.Date d =null;
        try {
            d = dateFormat.parse(r.getString("OrderDate"));
        } catch (ParseException ex) {
            Logger.getLogger(TableOrders.class.getName()).log(Level.SEVERE, null, ex);
        }
     Date sqlD = new Date(d.getYear(),d.getMonth(),d.getDay());
     return new Orders(dataObjectId,sqlD,r.getInt("CustomerId"),r.getInt("EmployeeId"),r.getInt("ShipperId"));
    }

    @Override
    public List<Orders> findAll(Connection con) throws SQLException {
     List<Orders> ret = new ArrayList<Orders>();
     
         String sql = "  Select * from Orders ; " ;
       PreparedStatement stm = con.prepareStatement(sql);
     
     stm.execute();
     ResultSet r = stm.getResultSet();
     while(r.next())
     {
     DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
      java.util.Date d =null;
        try {
            d = dateFormat.parse(r.getString("OrderDate"));
        } catch (ParseException ex) {
            Logger.getLogger(TableOrders.class.getName()).log(Level.SEVERE, null, ex);
        }
     Date sqlD = new Date(d.getYear(),d.getMonth(),d.getDay());
     ret.add(new Orders(r.getInt("orderId"),sqlD,r.getInt("CustomerId"),r.getInt("EmployeeId"),r.getInt("ShipperId")));
    }
    
     return ret;
    }
    
}
