/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.singidunum.projectKDP.deo.Employees;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nikola
 */
public class TableEmployees implements Dao<Employees>{

    @Override
    public void create(Connection con, Employees dataObject) throws SQLException {
      String sql = "INSERT INTO Employees(EmployeeId,LastName,FirstName,BirthDate) VALUES (?,?,?,?)" ;
      PreparedStatement stm = con.prepareStatement(sql);
      stm.setInt(1, dataObject.getEmployeeId());
      stm.setString(2, dataObject.getLastName());
      stm.setString(3, dataObject.getFirstName());
      stm.setDate(4, dataObject.getBirthDate());
      stm.execute();  
    }

    @Override
    public void update(Connection con, Employees dataObject) throws SQLException {
    String sql = "  UPDATE Employees  SET  EmployeeId=?,LastName=?,FirstName=?,BirthDate=? WHERE EmployeeId=? ";
    PreparedStatement stm = con.prepareStatement(sql);
    stm.setInt(1,dataObject.getEmployeeId());
    stm.setString(2,dataObject.getLastName());
    stm.setString(3,dataObject.getFirstName());
    stm.setString(4,dataObject.getBirthDate().toString());
    stm.setInt(5,dataObject.getEmployeeId());
    stm.execute();
    }

    @Override
    public void delete(Connection con, int dataObjectId) throws SQLException {
        String sql = "DELETE from Employees  where EmployeeId= ?" ;
        PreparedStatement stm = con.prepareStatement(sql);
        stm.setInt(1,dataObjectId);
        stm.execute();
        
 
    }

    @Override
    public Employees find(Connection con, int dataObjectId) throws SQLException {
        String sql="Select * from Employees where EmployeeId = ? ;";
        PreparedStatement stm = con.prepareStatement(sql);
        stm.setInt(1, dataObjectId);
        stm.execute();
        ResultSet r = stm.getResultSet();
        if( !r.isBeforeFirst() && r.getRow() == 0 )
            return null;
        r.next();
        return new Employees(dataObjectId,r.getString("LastName"),r.getString("FirstName"),r.getDate("BirthDate"));
        
    }

    @Override
    public List<Employees> findAll(Connection con) throws SQLException {
      List<Employees> ret = new ArrayList<Employees>();
       String sql="Select * from Employees ;";
       PreparedStatement stm = con.prepareStatement(sql);
       stm.execute();
       ResultSet r = stm.getResultSet();
       while(r.next())
       {
           ret.add(new Employees(r.getInt("EmployeeId"),r.getString("LastName"),r.getString("FirstName"),r.getDate("BirthDate")));
       }
      return ret;
    }
    
}
