/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.singidunum.projectKDP.deo.Shippers;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nikola
 */
public class TableShippers implements Dao<Shippers>{

    @Override
    public void create(Connection con, Shippers dataObject) throws SQLException {
    String sql = "INSERT INTO Shippers(ShipperId,ShipperName,Phone) VALUES (?,?,?);";
    PreparedStatement stm = con.prepareStatement(sql);
    stm.setInt(1, dataObject.getShipperId());
    stm.setString(2, dataObject.getShipperName());
    stm.setInt(3, dataObject.getPhone());
    stm.execute();   
    }

    @Override
    public void update(Connection con, Shippers dataObject) throws SQLException {
     
    String sql = " UPDATE Shippers  SET  ShipperId=?,ShipperName=?,Phone=? WHERE ShipperId=? ;";
      PreparedStatement stm = con.prepareStatement(sql);
      stm.setInt(1,dataObject.getShipperId());
      stm.setString(2,dataObject.getShipperName());
      stm.setInt(3,dataObject.getPhone());
      stm.setInt(4,dataObject.getShipperId());
      stm.execute();
        
    }

    @Override
    public void delete(Connection con, int dataObjectId) throws SQLException {
       String sql = "DELETE from Shippers  where ShipperId= ? ;";
       PreparedStatement stm = con.prepareStatement(sql);
       stm.setInt(1,dataObjectId);
       stm.execute();
        
        
    }

    @Override
    public Shippers find(Connection con, int dataObjectId) throws SQLException {
     
    String sql =  "Select * from Shippers where ShipperId = ? ;";
    PreparedStatement stm = con.prepareStatement(sql);
    stm.setInt(1, dataObjectId);
    stm.execute();
    ResultSet r = stm.getResultSet();
    if( !r.isBeforeFirst() && r.getRow() == 0 )
       return null;
    r.next();
    
    return new Shippers(dataObjectId,r.getString("ShipperName"),r.getInt("Phone"));
        
    }

    @Override
    public List<Shippers> findAll(Connection con) throws SQLException {
       List<Shippers> ret = new ArrayList<Shippers>();
       String sql =  "Select * from Shippers ;";
       PreparedStatement stm = con.prepareStatement(sql);
       stm.execute();
       ResultSet r = stm.getResultSet();
       while(r.next())
       {
           ret.add(new Shippers(r.getInt("ShipperId"),r.getString("ShipperName"),r.getInt("Phone")));
       }
       return ret;
    }
    
}
