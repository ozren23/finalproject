/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.singidunum.projectKDP.deo.DataClass;
import java.sql.*;
import java.util.List;

/**
 *
 * @author nikola
 */
public interface  Dao <T extends DataClass>  {
//    U bazu podataka dodaje novi dataObject čija je klasa implementirana u Data sloju.
    void create(Connection con, T dataObject) throws SQLException ;
//    Ažurira podatke o nekom zapisu u bazi podataka.
    void update(Connection con, T dataObject) throws SQLException ;
     
    void delete(Connection con, int dataObjectId) throws SQLException;
    
    T find(Connection con, int dataObjectId) throws SQLException;
    List<T> findAll(Connection con) throws SQLException;
}
