/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.singidunum.projectKDP.deo.Products;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nikola
 */
public class TableProducts implements Dao<Products>{

    @Override
    public void create(Connection con, Products dataObject) throws SQLException {
    String sql = "INSERT INTO Products(ProductId,ProductName,SuplierId,ProductCategory,PricePerUnit) VALUES (?,?,?,?,?);";
    PreparedStatement stm = con.prepareStatement(sql);
    stm.setInt(1, dataObject.getProductId());
    stm.setString(2, dataObject.getProductName());
    stm.setInt(3, dataObject.getSupplierId());
    stm.setString(4, dataObject.getProductCategory());
    stm.setDouble(5, dataObject.getPricePerUnit());
    stm.execute();
    }

    @Override
    public void update(Connection con, Products dataObject) throws SQLException {
    String sql =  "  UPDATE Products  SET  ProductId=?,ProductName=?,SuplierId=?,ProductCategory=?,PricePerUnit=? WHERE ProductId=? ;";
    PreparedStatement stm = con.prepareStatement(sql);
    stm.setInt(1,dataObject.getProductId());
    stm.setString(2,dataObject.getProductName());
    stm.setInt(3,dataObject.getSupplierId());
    stm.setString(4,dataObject.getProductCategory());
    stm.setDouble(5,dataObject.getPricePerUnit());
    stm.setInt(6,dataObject.getProductId());
stm.execute();
    }

    @Override
    public void delete(Connection con, int dataObjectId) throws SQLException {
       
    String sql = "DELETE from Products  where ProductId= ? ;";
    PreparedStatement stm = con.prepareStatement(sql);
    stm.setInt(1,dataObjectId);
    stm.execute();  
    }

    @Override
    public Products find(Connection con, int dataObjectId) throws SQLException {
       
        
       
       String sql =" Select * from Products where ProductId = ? ;" ;
       PreparedStatement stm = con.prepareStatement(sql);
       stm.setInt(1, dataObjectId);
       stm.execute();
       ResultSet r = stm.getResultSet();
       if( !r.isBeforeFirst() && r.getRow() == 0 ) 
           return null;
       r.next();
       return new Products(dataObjectId,r.getString("ProductName"),r.getInt("SuplierId"),r.getString("ProductCategory"),r.getDouble("PricePerUnit"));
    }

    @Override
    public List<Products> findAll(Connection con) throws SQLException {
       List<Products> ret = new ArrayList<Products>();
       String sql =" Select * from Products ;" ;
       PreparedStatement stm = con.prepareStatement(sql);
       stm.execute();
       ResultSet r = stm.getResultSet();
       while(r.next())
       {
           ret.add(new Products(r.getInt("ProductId"),r.getString("ProductName"),r.getInt("SuplierId"),r.getString("ProductCategory"),r.getDouble("PricePerUnit")));
       }
       return  ret;
    }
    
}
