/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.singidunum.projectKDP.deo.OrderDetails;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nikola
 */
public class TableOrderDetails implements Dao<OrderDetails>{

    @Override
    public void create(Connection con, OrderDetails dataObject) throws SQLException {
     String sql = "INSERT INTO OrderDetails(orderDetailsId,orderId,productId,quantity) VALUES (?,?,?,?)";
     PreparedStatement stm = con.prepareStatement(sql);
     stm.setInt(1, dataObject.getOrderDetailsId());
     stm.setInt(2, dataObject.getOrderId());
     stm.setInt(3, dataObject.getProductId());
     stm.setInt(4, dataObject.getQuantity());
     stm.execute();  
    }

    @Override
    public void update(Connection con, OrderDetails dataObject) throws SQLException {
       String sql = "UPDATE OrderDetails  SET  OrderDetailsId=?,OrderId=?,ProductId=?,Quantity=? WHERE OrderDetailsId=? ;";
       PreparedStatement stm = con.prepareStatement(sql);
        stm.setInt(1,dataObject.getOrderDetailsId());
        stm.setInt(2,dataObject.getOrderId());
        stm.setInt(3,dataObject.getProductId());
        stm.setInt(4,dataObject.getQuantity());
        stm.setInt(5,dataObject.getOrderDetailsId());
        stm.execute();
    }

    @Override
    public void delete(Connection con, int dataObjectId) throws SQLException {
        String sql = "DELETE from OrderDetails  where OrderDetailsId= ? ;";
        PreparedStatement stm = con.prepareStatement(sql);
        stm.setInt(1,dataObjectId);
        stm.execute();
    }

    @Override
    public OrderDetails find(Connection con, int dataObjectId) throws SQLException {
        String sql =" Select * from OrderDetails where OrderDetailsId = ? ";
        PreparedStatement stm = con.prepareStatement(sql);
        stm.setInt(1, dataObjectId);
        stm.execute();
        ResultSet r = stm.getResultSet();
        if( !r.isBeforeFirst() && r.getRow() == 0 )
            return null;
        r.next();
        return new OrderDetails(dataObjectId,r.getInt("OrderId"),r.getInt("ProductId"),r.getInt("Quantity"));
    }

    @Override
    public List<OrderDetails> findAll(Connection con) throws SQLException {
        List<OrderDetails> ret = new ArrayList<OrderDetails>();
        String sql =" Select * from OrderDetails ;";
        PreparedStatement stm = con.prepareStatement(sql);

        stm.execute();
        ResultSet r = stm.getResultSet();
        while(r.next())
        {
            ret.add(new OrderDetails(r.getInt("OrderDetailsId"),r.getInt("OrderId"),r.getInt("ProductId"),r.getInt("Quantity")));
        }
        return ret;
    }
    
}
