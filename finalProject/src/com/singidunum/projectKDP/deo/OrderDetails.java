/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.deo;

/**
 *
 * @author nikola
 */
//OrderDetails(polja: OrderDetailsId, OrderId, ProductId, Quantity)
final public class OrderDetails extends DataClass {
    
    private int orderDetailsId;
    private int orderId;
    private int productId;
    private int quantity;
    
    public OrderDetails(int orderDetailsId, int orderId, int productId, int quantity)
    {
        this.orderDetailsId = orderDetailsId;
        this.orderId = orderId;
        this.productId = productId;
        this.quantity = quantity;
    }

    public int getOrderDetailsId() {
        return orderDetailsId;
    }

    public void setOrderDetailsId(int orderDetailsId) {
        this.orderDetailsId = orderDetailsId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    @Override
    public String toString() {
        return "OrderDetails{" + "order details id=" + orderDetailsId + ", order id=" + orderId + ", product id=" + productId + ", quantity=" + quantity + '}';
    }
    
}
