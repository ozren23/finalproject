/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.deo;

/**
 *
 * @author nikola
 */
//polja: SupplierId, SupplierName, ContactPerson, Address, City, PostCode,
//Country, Phone)
final public class Suppliers extends DataClass {
  
    private int supplierId;
    private String supplierName;
    private String contactPerson;
    private String address;
    private String city;
    private String postCode;
    public Suppliers(int supplierId, String supplierName, String contactPerson, String address, String city, String postCode)
    {
        this.supplierId = supplierId;
        this.supplierName = supplierName;
        this.address = address;
        this.city = city;
        this.postCode = postCode;
        this.contactPerson = contactPerson;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }
    
    @Override
    public String toString() {
        return "Suppliers{" + "supplier id=" + supplierId + ", supplier name=" + supplierName + ", contact person=" + contactPerson + ", address=" + address + ", city=" + city + ", postal code=" + postCode +  '}';
    }
    
}
