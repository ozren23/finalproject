/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.deo;

import java.sql.Date;

/**
 *
 * @author nikola
 */
//EmployeeId, LastName, FirstName, BirthDate)
final public class Employees extends DataClass {
  private int employeeId;
  private String lastName;
  private String firstName;
  private Date birthDate;
  public Employees(int employeeId, String lastName, String firstName, Date birthDate)
  {
      this.employeeId=employeeId;
      this.lastName =lastName;
      this.firstName = firstName;
      this.birthDate = birthDate;
  }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    
    @Override
    public String toString() {
        return "Employees{" + "employee id=" + employeeId + ", last name=" + lastName + ", first name=" + firstName + ", birth date=" + birthDate + '}';
    }
    
}
