/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.deo;

/**
 *
 * @author nikola
 */
//Products(polja: ProductId, ProductName, SuplierID, ProductCategory, PricePerUnit)
final public class Products extends DataClass {
    
    private int productId;
    private String productName;
    private int supplierId;
    private String productCategory;
    private double pricePerUnit;
    public Products(int productId, String productName, int supplierId, String productCategory, double pricePerUnit)
    {
        this.productId = productId;
        this.pricePerUnit = pricePerUnit;
        this.productCategory = productCategory;
        this.productName = productName;
        this.supplierId = supplierId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }
    
    @Override
    public String toString() {
        return "Products{" + "product id=" + productId + ", product name=" + productName + ", supplier id=" + supplierId + ", product category=" + productCategory + ", price per unit=" + pricePerUnit + '}';
    }

}
