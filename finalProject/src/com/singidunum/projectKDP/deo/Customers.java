/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.deo;

/**
 *
 * @author nikola
 */
//Customers(polja: CustomerId, CustomerName, ContactPerson, Address, City,
//PostCode, Country)
final public class Customers extends DataClass{
  private int customerId;
  private String customerName;
  private String contractPerson;
  private String address;
  private String city;
  private int postCode = 23;

    /**
     * @return the customerId
     */
    public int getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId the customerId to set
     */
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the contractPerson
     */
    public String getContractPerson() {
        return contractPerson;
    }

    /**
     * @param contractPerson the contractPerson to set
     */
    public void setContractPerson(String contractPerson) {
        this.contractPerson = contractPerson;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }
    
    public Customers()
    {
//        setCustomerId(customerId);
//        setCustomerName(customerName);
//        setContractPerson(contractPerson);
//        setAddress(address);
//        setCity(city);   
    }

    public Customers(int customerId, String customerName, String contractPerson, String address, String city,int postCode)
    {
        
        this.customerId = customerId;
        this.customerName = customerName;
        this.contractPerson=contractPerson;
        this.address = address;
        this.city=city;
        this.postCode = postCode;
    }
    
    

    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }
    
    @Override
    public String toString() {
        return "Customers{" + "customer id=" + customerId + ", customer name=" + customerName + ", contact person=" + contractPerson + ", address=" + address + ", city=" + city + ", postal code=" + postCode  + '}';
    }

 
    
    
}
