/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.deo;

import java.sql.Date;

/**
 *
 * @author nikola
 */
//Orders(polja: OrderId, OrderDate, CustomerId, EmployeeId, ShipperId)

final public class Orders extends DataClass {
   private int orderId;
   private Date orderDate;
   private int customerId;
   private int employeeId;
   private int shipperId;
   
   public Orders(int orederId, Date orderDate, int customerId, int employeedId, int shipperId)
   {
       this.customerId = customerId;
       this.orderDate = orderDate;
       this.customerId = customerId;
       this.employeeId = employeedId;
       this.shipperId = shipperId;
   }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getShipperId() {
        return shipperId;
    }

    public void setShipperId(int shipperId) {
        this.shipperId = shipperId;
    }

     @Override
    public String toString() {
        return "Orders{" + "order id=" + orderId + ", order date=" + orderDate + ", customer id=" + customerId + ", employee id=" + employeeId + ", shipper id=" + shipperId + '}';
    } 
           
}
