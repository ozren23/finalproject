/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.service;

import com.singidunum.projectKDP.deo.Employees;
import com.singidunum.projectKDP.deo.Products;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author nikola
 */
public class Service {
    
    
//    select * from Customers,Orders
//where Orders.CustomerId = Customers.CustomerId;
//    
//Izlistati svakog Customer-a i sve njihove Order-e jednog ispod drugog u formatu
//CustomerName OrderId
    public static void getCustomersAndOrders(Connection con) throws SQLException
    {
        String sql = " select CustomerName,OrderId from Customers,Orders\n" +
        "where Orders.CustomerId = Customers.CustomerId\n" +
        "ORDER By CustomerName asc;";
        PreparedStatement stm = con.prepareStatement(sql);
        stm.execute();
        ResultSet r= stm.getResultSet();
        System.out.println( "CustomerName " + " OrderId" );
        while(r.next())
        {
           System.out.println(r.getString("CustomerName") +" "+r.getInt("OrderId"));
        }
    }
    
    //Izlistati sve Product-e za čije dobavljanje je nadležan Supplier sa datim SupplierId
    public static void getProducts(Connection con,int supplierId) throws SQLException 
    {
        String sql = "select * from Products where suplierId = ?;" ;
        PreparedStatement stm = con.prepareStatement(sql);   
        stm.setInt(1, supplierId);
        stm.execute();
        ResultSet r= stm.getResultSet();
        while(r.next())
        {
        Products tmp = new Products(supplierId,r.getString("ProductName"),r.getInt("SuplierId"),r.getString("ProductCategory"),r.getDouble("PricePerUnit"));
        System.out.println( tmp.toString()  );
        
        }
    }
    
    public static void getProudctsPrice(Connection con) throws SQLException
    {
        String sql = "select * FROM OrderDetails,Products\n" +
        " where OrderDetails.ProductId = Products.ProductId" ;
        PreparedStatement stm = con.prepareStatement(sql);
        stm.execute();
        ResultSet r= stm.getResultSet();
        double suma = 0;
        while(r.next())
        {
            suma +=  r.getDouble("Quantity") * r.getDouble("PricePerUnit");
        }
        System.out.println("Ukupna suma je: "+ suma);
    }
    //Izračunati cenu narudžbina (Order-a) koje je naručio Customer sa datim CustomerId
    public static void getProductsPriceForCustomer(Connection con, int customerId) throws SQLException
    {
               String sql = "select * from Products,Orders,OrderDetails \n" +
        "where Orders.OrderId = OrderDetails.OrderId and OrderDetails.ProductId = Products.ProductId " +
        "and CustomerId = ?";
        PreparedStatement stm = con.prepareStatement(sql);
        stm.setInt(1, customerId);
        stm.execute();
        ResultSet r= stm.getResultSet();
        double suma = 0;
        while(r.next())
        {
            suma +=  r.getDouble("Quantity") * r.getDouble("PricePerUnit");
        }
        System.out.println("Ukupna suma je: "+ suma); 
    }
    public static void getProductsPriceForShipper(Connection con, int shipperId) throws SQLException
    {
              String sql = "select * from Products,Orders,OrderDetails " +
              "where Orders.OrderId = OrderDetails.OrderId and OrderDetails.ProductId = Products.ProductId " +
              "and ShipperId = ?";

        PreparedStatement stm = con.prepareStatement(sql);
        stm.setInt(1, shipperId);
        stm.execute();
        ResultSet r= stm.getResultSet();
        double suma = 0;
        while(r.next())
        {
            suma +=  r.getDouble("Quantity") * r.getDouble("PricePerUnit");
        }
        System.out.println("Ukupna suma je: "+ suma); 
    }
    
    public static void getProductsPriceForSupplier(Connection con, int supplierId) throws SQLException
    {
                     String sql = "select * from Products,Orders,OrderDetails " +
              "where Orders.OrderId = OrderDetails.OrderId and OrderDetails.ProductId = Products.ProductId " +
              "and SuplierID = ?";

        PreparedStatement stm = con.prepareStatement(sql);
        stm.setInt(1, supplierId);
        stm.execute();
        ResultSet r= stm.getResultSet();
        double suma = 0;
        while(r.next())
        {
            suma +=  r.getDouble("Quantity") * r.getDouble("PricePerUnit");
        }
        System.out.println("Ukupna suma je: "+ suma);  
    }
    
    public static void getEmployesWithHighestProructPrice(Connection con) throws SQLException
    {
           String sql =  " select sum(Quantity * PricePerUnit) as Cene from  Products,Orders,OrderDetails,Employees "+
    "where (Orders.OrderId = OrderDetails.OrderId) and (OrderDetails.ProductId = Products.ProductId) " +
    "and (Orders.EmployeeId = Employees.EmployeeId)" +
    "GROUP by Employees.EmployeeId " +
   " order by Cene desc" ;


        PreparedStatement stm = con.prepareStatement(sql);
       
        stm.execute();
        ResultSet r= stm.getResultSet();
        double suma = 0;
        if(r.next())
          System.out.println(  (new Employees(r.getInt("Employees.EmployeeId"),r.getString("LastName"),r.getString("FirstName"),r.getDate("BirthDate"))).toString());
        
        
    }
    public static void get3MostTakenProducts(Connection con) throws SQLException
    {
     
                  String sql = "select count(*) as narudzbina, Products.* " +
                    "from Products,OrderDetails " +
            " where Products.ProductId = OrderDetails.ProductId " +
            " group by (Products.ProductId) " + 
            " order by narudzbina asc  ; " ;


        PreparedStatement stm = con.prepareStatement(sql);
       
        stm.execute();
        ResultSet r= stm.getResultSet();
        double suma = 0;
        
        int i=0;
        while((r.next())&& (i++<3))
        {
            System.out.println(new Products(r.getInt("ProductId"),r.getString("ProductName"),r.getInt("SuplierId"),r.getString("ProductCategory"),r.getDouble("PricePerUnit")).toString());
         }
        
    }
}
